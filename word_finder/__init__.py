import builtins
import re
from collections import defaultdict
from decimal import *

from pyspark.sql import SQLContext
from pyspark.sql.functions import *
from pyspark.sql.types import *

getcontext().prec = 2


def count_appearances(a, b):
    data = a + b
    d = defaultdict(int)
    for word, value in data:
        d[word] += value
    return list(d.items())


def scoring(x, weight_dict, total_wight):
    score = 0
    for word in x[1][1]:
        score += weight_dict[word]
    return builtins.round((score / total_wight) * 100, 2)


class WordsFinder:
    def __init__(self, list_of_files):
        self.data = []
        self.sc = SparkContext("local", "app")
        self.sc.setLogLevel("OFF")
        self.sql_context = SQLContext(self.sc)
        self.schema = [StructField("file", StringType(), False), StructField("score", FloatType(), False),
                       StructField("number_of_matches", IntegerType(), False)]
        self.list_of_files = list_of_files

    def invert_index(self, path, list_of_files):
        rdd = self.sc.wholeTextFiles(path)
        inverted_index = rdd.flatMap(
            lambda file: [(re.sub('[^A-Za-z0-9]+', '', word), [(file[0].split('/')[-1], 1)]) for word in
                          file[1].lower().split()]) \
            .reduceByKey(count_appearances) \
            .map(lambda x: (x[0], x[1], len(list_of_files) / len(x[1])))
        inverted_index.count()  # Trigger computations
        return inverted_index

    def get_files_scores(self, inverted_index, words, file_names):
        len_words_searched = len(words)
        words = [re.sub('[^A-Za-z0-9]+', '', item) for item in words]
        words_in_files = inverted_index.filter(lambda x: x[0] in words)
        len_words_found = words_in_files.count()
        if len_words_found == 0:
            print("No matches found!")
            return

        weight_for_existing_words = words_in_files.map(lambda x: (x[0], x[2])).values().sum()  # sum of all weights
        total_weight = weight_for_existing_words * len_words_searched / len_words_found
        weights_per_word = words_in_files.map(lambda x: (x[0], x[2])).collectAsMap()

        file_matches = words_in_files.flatMapValues(lambda x: x) \
            .flatMap(lambda x: [(x[1][0], [x[1][1], [x[0]]])])

        file_matches_list = file_matches.keys().collect()

        scores = file_matches.reduceByKey(lambda x, y: (x[0] + y[0], x[1] + y[1])) \
            .map(lambda x: (x[0], scoring(x, weights_per_word, total_weight), x[1][0]))
        df = self.sql_context.createDataFrame(scores, schema=StructType(self.schema))
        df.createOrReplaceTempView("files_with_matches")

        print("Weights used for calculations: " + str(weights_per_word))

        print('Results: \n')
        best_files = df.orderBy(['score', 'number_of_matches'], ascending=False).take(10)
        for file in best_files:
            print(file[0] + ': ' + str(builtins.round(file[1], 2)) + '%')

        if len(file_matches_list) < 10:
            results_showed = len(file_matches_list)
            for file_name in file_names:
                if results_showed == 10:
                    break
                if file_name not in file_matches_list and results_showed < 10:
                    print(file_name + ': 0%')

        return None
