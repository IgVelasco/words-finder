
FROM python:3.8-slim-buster AS py3
FROM openjdk:8-slim-buster

WORKDIR /home

COPY --from=py3 / /
COPY requirements.txt .

ARG PYSPARK_VERSION=3.1.2
RUN pip install -r requirements.txt

# command to run on container start
RUN echo $PATH
RUN ls /home
ENTRYPOINT [ "python3", "app.py"]