#!/usr/bin/python
import getopt
import sys
from os import path, listdir
from os.path import isfile, join
from word_finder import WordsFinder
import re


import time



def main(argv):
    files_path = ''
    try:
        opts, args = getopt.getopt(argv, "hi:o:", ["ifile=", "ofile="])
    except getopt.GetoptError:
        print('Run using: python3 app.py <PATH-TO-SEARCH>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('Run using: python3 app.py <PATH-TO-SEARCH>')
            sys.exit()
    if len(args) == 0:
        print('Run using: python3 app.py <PATH-TO-SEARCH>')
        sys.exit(2)
    files_path = args[0]
    if not path.exists(files_path):
        print('Please provide a valid path')
        sys.exit(3)

    file_list = [f for f in listdir(files_path) if isfile(join(files_path, f))]

    utils = WordsFinder(file_list)
    print("Inverting index...")
    inverted_index = utils.invert_index(files_path, file_list)
    while True:
        words = input('\nsearch> ')
        if words == ':quit':
            sys.exit()
        elif words == ':change-path':
            files_path = input('\ninsert new path> ')
            file_list = [f for f in listdir(files_path) if isfile(join(files_path, f))]
            inverted_index = utils.invert_index(files_path, file_list)
        else:
            print("The words to search are:  " + str(list(set(words.lower().split()))))
            words_to_search = [re.sub('[^A-Za-z0-9]+', '', word) for word in words.lower().split()]
            start_time = time.time()
            utils.get_files_scores(inverted_index, list(set(words_to_search)), file_list)
            print("--- %s seconds ---" % (time.time() - start_time))


if __name__ == "__main__":
    main(sys.argv[1:])
