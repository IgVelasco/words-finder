
# Words-Finder

This project consist in a multiple word finder for files inside a path that returns the score of the top 10 files, it was made as a challenge for a data science position for Livestories

## Observations:
1. The first run will be slower than the following ones
2. There is a command `:change-path` to update the path you choose to search words.
3. The file `testFiles.zip` contains some files that I used for testing, feel free to use them.
4. For the docker version the directory needs to be inside the code directory!

## Installation and running


### Download the code
1. Clone the project

```bash
  git clone https://gitlab.com/IgVelasco/words-finder.git
```
2. Go to the project directory

```bash
  cd words-finder
```

### Docker version:
- For this version the directory needs to be inside the code directory!)
- You need docker-compose to run it in this way   

1. Build containers
```bash
docker-compose build
```

2. Run!

```bash
docker-compose run app <PATH-TO-SEARCH> 
```




### Without docker
You need JAVA, python3,  and python3-venv already installed in order to run correctly
#### Install dependencies (Bash script `setUp`)




1. Create virtual environment for python 
```bash
  python3 -m venv ./venv
```

2. Activate virtual environment
```bash
  source venv/bin/activate
```

3. Install requirements

```bash
  pip install -r requirements.txt
```

4. Run indicating the path as parameter
   (Bash script `simpleSearch`)
```bash
python3 app.py <PATH-TO-SEARCH>
```



## Solution
### Things considered in my implementation
#### What constitutes a word?
A word is a set of continuous letters that don't have a space in between them

#### What constitutes two words being equal (and matching)?

So with this definition I decide to choose a quite simple approach, words match when they have the same letters,
but they can't have special characters, each special character was replaced by nothing which helped in matching words
like 'hello' and 'hello.'

I used this regular expression: `[^A-Za-z0-9]`, and set all word letters to lowercase that help with words like 'Bye'
and 'bye'

#### Data structure design: the in memory representation to search against
I used an inverted index, it was quite fun to implement. Here is a following print of a small inverted index generated with my code:
```

[('this', [('file1', 2), ('THIS', 1), ('file2', 1)], 1.0), ('is', [('file1', 1)], 3.0)]

Structure: (word, [(file_that_contains_word, appearances)], weight_of_word ) 
```

#### Ranking score design:
So for this part I design some simple equations to give a "rarity_weight" to each word

**Weight of word found:**

![image-20210915231520149](images/equation_words_found.png)



 **Total weight:**

![image-20210915231600421](images/equations_for_total_weight.png)

**And finally the score:**

If the word is in the file it sums the rarity weight of the matching word 



![image-20210915231641689](images/equations_for_score.png)









![image-20210915231735920](images/example.png)









  



